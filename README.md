## University Management System
UMS is a minimal system which can hold Professors, Students, Courses and Grades information of a university.

**Admins** can manage data and see or manipulate data, add new students, professors and courses or edit details.

**Students** can see their grades in courses and **Professors** can give grades to students in courses.

#### Idea
This was my university project for *Advanced Programming* course.

Teacher: Ardalan Ghasemzadeh

## Tech Stack
- C#
- Windows Forms
- MySql

### Developer
- Seyed Mehran Abghari (mehran.ab80@gmail.com)

### License
MIT
