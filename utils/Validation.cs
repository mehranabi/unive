﻿namespace SamaSystem.utils
{
    class Validation
    {
        public static bool numeric(string value)
        {
            return Utils.isNumeric(value);
        }

        public static bool max(string value, int validation)
        {
            return value.Length <= validation;
        }
        public static bool max(int value, int validation)
        {
            return value <= validation;
        }

        public static bool min(string value, int validation)
        {
            return value.Length >= validation;
        }
        public static bool min(int value, int validation)
        {
            return value >= validation;
        }

        public static bool Username(string username)
        {
            return min(username, 3) && max(username, 30);
        }

        public static bool Password(string password)
        {
            return min(password, 6) && max(password, 30);
        }

        public static bool CodeMelli(string codeMelli)
        {
            return numeric(codeMelli) && min(codeMelli, 10) && max(codeMelli, 10);
        }

        public static bool Name(string name)
        {
            return min(name, 2) && max(name, 30);
        }

        public static bool StudentCode(string code)
        {
            return numeric(code) && min(code, 9) && max(code, 9);
        }

        public static bool TeacherCode(string code)
        {
            return numeric(code) && min(code, 5) && max(code, 15);
        }

        public static bool Grade(byte grade)
        {
            return max(grade, 20);
        }
    }
}
