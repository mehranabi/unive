﻿using SamaSystem.models;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    public interface Repository<T> where T : Model
    {
        List<T> all();
        T find(int id);
        T find(string prop, object val);
        void insert(params object[] list);
        void update(int id, params object[] list);
        void delete(int id);
        void deleteAt(int index);
    }
}
