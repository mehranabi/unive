﻿using MySql.Data.MySqlClient;
using SamaSystem.models;
using SamaSystem.services;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class UsersRepository : Repository<User>
    {
        private static UsersRepository repository = null;

        public static UsersRepository Get()
        {
            if (repository == null)
            {
                repository = new UsersRepository();
            }

            return repository;
        }

        private UsersRepository()
        {
            Program.database.Write(Queries.CREATE_USERS_TABLE);

            User user = find("code_melli", "1111111111");
            if (user == null) {
                insert("1111111111", "admin", "123456", "Admin");
            }
        }

        public List<User> all()
        {
            List<User> users = new List<User>();
            
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ALL, Queries.USERS_TABLE_NAME));
            while (reader.Read())
            {
                users.Add(User.make((int)reader["id"], (string)reader["code_melli"], (string)reader["username"], (string)reader["password"], (string)reader["type"]));
            }
            reader.Close();

            return users;
        }

        public void delete(int id)
        {
            Program.database.Write(string.Format(Queries.DELETE, Queries.USERS_TABLE_NAME, id));
        }

        public void deleteAt(int index)
        {
            List<User> users = all();
            int id = users[index].getId();
            delete(id);
        }

        public User find(int id)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE, Queries.USERS_TABLE_NAME, id));
            
            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            User user = User.make((int)reader["id"], (string)reader["code_melli"], (string)reader["username"], (string)reader["password"], (string)reader["type"]);

            reader.Close();

            return user;
        }

        public User find(string prop, object val)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE_BY, Queries.USERS_TABLE_NAME, prop, val));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            User user = User.make((int)reader["id"], (string)reader["code_melli"], (string)reader["username"], (string)reader["password"], (string)reader["type"]);

            reader.Close();

            return user;
        }

        public void insert(params object[] list)
        {
            Program.database.Write(string.Format(Queries.INSERT_USER, list[0], list[1], list[2], list[3]));
        }

        public void update(int id, params object[] list)
        {
            Program.database.Write(string.Format(Queries.UPDATE_USER, id, list[0], list[1], list[2], list[3]));
        }
    }
}
