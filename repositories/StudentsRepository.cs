﻿using MySql.Data.MySqlClient;
using SamaSystem.models;
using SamaSystem.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamaSystem.repositories
{
    class StudentsRepository : Repository<Student>
    {
        private static StudentsRepository repository = null;

        public static StudentsRepository Get()
        {
            if (repository == null)
            {
                repository = new StudentsRepository();
            }

            return repository;
        }

        private StudentsRepository()
        {
            Program.database.Write(Queries.CREATE_STUDENTS_TABLE);
        }

        public List<Student> all()
        {
            List<Student> students = new List<Student>();

            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ALL, Queries.STUDENTS_TABLE_NAME));
            while (reader.Read())
            {
                students.Add(Student.make((int)reader["id"], (string)reader["code_melli"], (string)reader["first_name"], (string)reader["last_name"], (string)reader["code"], (string)reader["field"]));
            }
            reader.Close();

            return students;
        }

        public void delete(int id)
        {
            Program.database.Write(string.Format(Queries.DELETE, Queries.STUDENTS_TABLE_NAME, id));
        }

        public void deleteAt(int index)
        {
            List<Student> students = all();
            int id = students[index].getId();
            delete(id);
        }

        public Student find(int id)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE, Queries.STUDENTS_TABLE_NAME, id));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            Student student = Student.make((int)reader["id"], (string)reader["code_melli"], (string)reader["first_name"], (string)reader["last_name"], (string)reader["code"], (string)reader["field"]);

            reader.Close();

            return student;
        }

        public Student find(string prop, object val)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE_BY, Queries.STUDENTS_TABLE_NAME, prop, val));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            Student student = Student.make((int)reader["id"], (string)reader["code_melli"], (string)reader["first_name"], (string)reader["last_name"], (string)reader["code"], (string)reader["field"]);

            reader.Close();

            return student;
        }

        public void insert(params object[] list)
        {
            Program.database.Write(string.Format(Queries.INSERT_STUDENT, list[0], list[1], list[2], list[3], list[4]));
        }

        public void update(int id, params object[] list)
        {
            throw new NotImplementedException();
        }
    }
}
