﻿using MySql.Data.MySqlClient;
using SamaSystem.models;
using SamaSystem.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamaSystem.repositories
{
    class CoursesRepository : Repository<Course>
    {
        private static CoursesRepository repository = null;

        public static CoursesRepository Get()
        {
            if (repository == null)
            {
                repository = new CoursesRepository();
            }

            return repository;
        }

        private CoursesRepository()
        {
            Program.database.Write(Queries.CREATE_COURSES_TABLE);
        }

        public List<Course> all()
        {
            List<Course> courses = new List<Course>();

            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ALL, Queries.COURSES_TABLE_NAME));
            while (reader.Read())
            {
                courses.Add(Course.make((int)reader["id"], (string)reader["name"], (string)reader["type"], (int)reader["unit"]));
            }
            reader.Close();

            return courses;
        }

        public void delete(int id)
        {
            Program.database.Write(string.Format(Queries.DELETE, Queries.COURSES_TABLE_NAME, id));
        }

        public void deleteAt(int index)
        {
            List<Course> courses = all();
            int id = courses[index].getId();
            delete(id);
        }

        public Course find(int id)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE, Queries.COURSES_TABLE_NAME, id));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            Course course = Course.make((int)reader["id"], (string)reader["name"], (string)reader["type"], (int)reader["unit"]);

            reader.Close();

            return course;
        }

        public Course find(string prop, object val)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE_BY, Queries.COURSES_TABLE_NAME, prop, val));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            Course course = Course.make((int)reader["id"], (string)reader["name"], (string)reader["type"], (int)reader["unit"]);

            reader.Close();

            return course;
        }

        public void insert(params object[] list)
        {
            Program.database.Write(string.Format(Queries.INSERT_COURSE, list[0], list[1], list[2]));
        }

        public void update(int id, params object[] list)
        {
            Program.database.Write(string.Format(Queries.UPDATE_COURSE, id, list[0], list[1], list[2]));
        }
    }
}
