﻿using MySql.Data.MySqlClient;
using SamaSystem.models;
using SamaSystem.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamaSystem.repositories
{
    class TeachersRepository : Repository<Teacher>
    {
        private static TeachersRepository repository = null;

        public static TeachersRepository Get()
        {
            if (repository == null)
            {
                repository = new TeachersRepository();
            }

            return repository;
        }

        private TeachersRepository()
        {
            Program.database.Write(Queries.CREATE_TEACHERS_TABLE);
        }

        public List<Teacher> all()
        {
            List<Teacher> teachers = new List<Teacher>();

            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ALL, Queries.TEACHERS_TABLE_NAME));
            while (reader.Read())
            {
                teachers.Add(Teacher.make((int)reader["id"], (string)reader["code_melli"], (string)reader["first_name"], (string)reader["last_name"], (string)reader["code"], (string)reader["level"], (int)reader["salary"]));
            }
            reader.Close();

            return teachers;
        }

        public void delete(int id)
        {
            Program.database.Write(string.Format(Queries.DELETE, Queries.TEACHERS_TABLE_NAME, id));
        }

        public void deleteAt(int index)
        {
            List<Teacher> teachers = all();
            int id = teachers[index].getId();
            delete(id);
        }

        public Teacher find(int id)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE, Queries.TEACHERS_TABLE_NAME, id));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            Teacher teacher = Teacher.make((int)reader["id"], (string)reader["code_melli"], (string)reader["first_name"], (string)reader["last_name"], (string)reader["code"], (string)reader["level"], (int)reader["salary"]);

            reader.Close();

            return teacher;
        }

        public Teacher find(string prop, object val)
        {
            MySqlDataReader reader = Program.database.Read(string.Format(Queries.SELECT_ONE_BY, Queries.TEACHERS_TABLE_NAME, prop, val));

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();

            Teacher teacher = Teacher.make((int)reader["id"], (string)reader["code_melli"], (string)reader["first_name"], (string)reader["last_name"], (string)reader["code"], (string)reader["level"], (int)reader["salary"]);

            reader.Close();

            return teacher;
        }

        public void insert(params object[] list)
        {
            Program.database.Write(string.Format(Queries.INSERT_TEACHER, list[0], list[1], list[2], list[3], list[4], list[5]));
        }

        public void update(int id, params object[] list)
        {
            throw new NotImplementedException();
        }
    }
}
