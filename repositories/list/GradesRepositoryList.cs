﻿using SamaSystem.models;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class GradesRepositoryList : Repository<Grade>
    {
        private static GradesRepositoryList repository = null;
        private List<Grade> grades;
        private int lastIndex = 1;

        public static GradesRepositoryList Get()
        {
            if (repository == null)
            {
                repository = new GradesRepositoryList();
            }

            return repository;
        }

        private GradesRepositoryList()
        {
            grades = new List<Grade>();
        }

        public List<Grade> all()
        {
            return grades;
        }

        public Grade find(int id)
        {
            foreach (Grade grade in grades)
            {
                if (grade.getId() == id)
                {
                    return grade;
                }
            }
            return null;
        }

        public Grade find(string prop, object val)
        {
            foreach (Grade grade in grades)
            {
                if (grade.get(prop).ToString() == val.ToString())
                {
                    return grade;
                }
            }
            return null;
        }

        public void insert(params object[] list)
        {
            grades.Add(Grade.make(lastIndex++, (Course)list[0], (Student)list[1], (Teacher)list[2], (string)list[3], (byte)list[4]));
        }

        public void update(int id, params object[] list)
        {
            Grade newGrade = null;

            foreach (Grade grade in grades)
            {
                if (grade.getId() == id)
                {
                    newGrade = grade;
                    grades.Remove(grade);
                    break;
                }
            }

            if (newGrade != null)
            {
                newGrade.setCourse((Course)list[0]);
                newGrade.setStudent((Student)list[1]);
                newGrade.setTeacher((Teacher)list[2]);
                newGrade.setSemester((string)list[3]);
                newGrade.setGrade((byte)list[4]);

                grades.Add(newGrade);
            }
        }

        public void delete(int id)
        {
            foreach (Grade grade in grades)
            {
                if (grade.getId() == id)
                {
                    grades.Remove(grade);
                }
            }
        }

        public void deleteAt(int index)
        {
            grades.RemoveAt(index);
        }
    }
}
