﻿using SamaSystem.models;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class CoursesRepositoryList : Repository<Course>
    {
        private static CoursesRepositoryList repository = null;
        private List<Course> courses;
        private int lastIndex = 1;

        public static CoursesRepositoryList Get()
        {
            if (repository == null)
            {
                repository = new CoursesRepositoryList();
            }

            return repository;
        }

        private CoursesRepositoryList()
        {
            courses = new List<Course>();

            insert("Advanced Programming", "Requried", (byte)3);
        }

        public List<Course> all()
        {
            return courses;
        }

        public Course find(int id)
        {
            foreach (Course course in courses)
            {
                if (course.getId() == id)
                {
                    return course;
                }
            }
            return null;
        }

        public Course find(string prop, object val)
        {
            foreach (Course course in courses)
            {
                if (course.get(prop).ToString() == val.ToString())
                {
                    return course;
                }
            }
            return null;
        }

        public void insert(params object[] list)
        {
            courses.Add(Course.make(lastIndex++, (string)list[0], (string)list[1], (byte)list[2]));
        }

        public void update(int id, params object[] list)
        {
            Course newCourse = null;

            foreach (Course course in courses)
            {
                if (course.getId() == id)
                {
                    newCourse = course;
                    courses.Remove(course);
                    break;
                }
            }

            if (newCourse != null)
            {
                newCourse.setName((string)list[0]);
                newCourse.setType((string)list[1]);
                newCourse.setUnit((byte)list[2]);

                courses.Add(newCourse);
            }
        }

        public void delete(int id)
        {
            foreach (Course course in courses)
            {
                if (course.getId() == id)
                {
                    courses.Remove(course);
                }
            }
        }

        public void deleteAt(int index)
        {
            courses.RemoveAt(index);
        }
    }
}
