﻿using SamaSystem.models;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class UsersRepositoryList : Repository<User>
    {
        private static UsersRepositoryList repository = null;
        private List<User> users;
        private int lastId = 1;

        public static UsersRepositoryList Get()
        {
            if (repository == null)
            {
                repository = new UsersRepositoryList();
            }

            return repository;
        }

        private UsersRepositoryList()
        {
            users = new List<User>();

            // Sample data
            insert("1285814551", "farhadabi", "123456", "Admin");
            insert("4421018503", "mehranabi", "123456", "Student");
            insert("1234567890", "ardalan", "123456", "Teacher");
        }

        public List<User> all()
        {
            return users;
        }

        public User find(int id)
        {
            foreach (User user in users)
            {
                if (user.getId() == id)
                {
                    return user;
                }
            }
            return null;
        }

        public User find(string prop, object val)
        {
            foreach (User user in users)
            {
                if (user.get(prop).ToString() == val.ToString())
                {
                    return user;
                }
            }
            return null;
        }

        public void insert(params object[] list)
        {
            users.Add(User.make(lastId++, (string)list[0], (string)list[1], (string)list[2], (string)list[3]));
        }

        public void update(int id, params object[] list)
        {
            User newUser = null;

            foreach (User user in users)
            {
                if (user.getId() == id)
                {
                    newUser = user;
                    users.Remove(user);
                    break;
                }
            }

            if (newUser != null)
            {
                newUser.setCodeMelli((string)list[0]);
                newUser.setUsername((string)list[1]);
                newUser.setPassword((string)list[2]);
                newUser.setType((string)list[3]);

                users.Add(newUser);
            }
        }

        public void delete(int id)
        {
            foreach (User user in users)
            {
                if (user.getId() == id)
                {
                    users.Remove(user);
                }
            }
        }

        public void deleteAt(int index)
        {
            users.RemoveAt(index);
        }
    }
}
