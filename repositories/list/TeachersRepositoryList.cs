﻿using SamaSystem.models;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class TeachersRepositoryList : Repository<Teacher>
    {
        private static TeachersRepositoryList repository = null;
        private List<Teacher> teachers;
        private int lastIndex = 1;

        public static TeachersRepositoryList Get()
        {
            if (repository == null)
            {
                repository = new TeachersRepositoryList();
            }

            return repository;
        }

        private TeachersRepositoryList()
        {
            teachers = new List<Teacher>();

            insert("1234567890", "Ardalan", "Ghasem Zadeh", "9876543210", "Ostad", (uint)5000000);
        }

        public List<Teacher> all()
        {
            return teachers;
        }

        public Teacher find(int id)
        {
            foreach (Teacher teacher in teachers)
            {
                if (teacher.getId() == id)
                {
                    return teacher;
                }
            }
            return null;
        }

        public Teacher find(string prop, object val)
        {
            foreach (Teacher teacher in teachers)
            {
                if (teacher.get(prop).ToString() == val.ToString())
                {
                    return teacher;
                }
            }
            return null;
        }

        public void insert(params object[] list)
        {
            teachers.Add(Teacher.make(
                lastIndex++,
                (string)list[0],
                (string)list[1],
                (string)list[2],
                (string)list[3],
                (string)list[4],
                (int)list[5]
            ));
        }

        public void update(int id, params object[] list)
        {
            Teacher newTeacher = null;

            foreach (Teacher teacher in teachers)
            {
                if (teacher.getId() == id)
                {
                    newTeacher = teacher;
                    teachers.Remove(teacher);
                    break;
                }
            }

            if (newTeacher != null)
            {
                newTeacher.setCodeMelli((string)list[0]);
                newTeacher.setFirstName((string)list[1]);
                newTeacher.setLastName((string)list[2]);
                newTeacher.setCode((string)list[3]);
                newTeacher.setLevel((string)list[4]);
                newTeacher.setSalary((uint)list[5]);

                teachers.Add(newTeacher);
            }
        }

        public void delete(int id)
        {
            foreach (Teacher teacher in teachers)
            {
                if (teacher.getId() == id)
                {
                    teachers.Remove(teacher);
                }
            }
        }

        public void deleteAt(int index)
        {
            teachers.RemoveAt(index);
        }
    }
}
