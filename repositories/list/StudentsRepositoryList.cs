﻿using SamaSystem.models;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class StudentsRepositoryList : Repository<Student>
    {
        private static StudentsRepositoryList repository = null;
        private List<Student> students;
        private int lastIndex = 1;

        public static StudentsRepositoryList Get()
        {
            if (repository == null)
            {
                repository = new StudentsRepositoryList();
            }

            return repository;
        }

        private StudentsRepositoryList()
        {
            students = new List<Student>();

            insert("4421018503", "Seyed Mehran", "Abghari", "981152138", "Computer-Engineering");
        }

        public List<Student> all()
        {
            return students;
        }

        public Student find(int id)
        {
            foreach (Student student in students)
            {
                if (student.getId() == id)
                {
                    return student;
                }
            }
            return null;
        }

        public Student find(string prop, object val)
        {
            foreach (Student student in students)
            {
                if (student.get(prop).ToString() == val.ToString())
                {
                    return student;
                }
            }
            return null;
        }

        public void insert(params object[] list)
        {
            students.Add(Student.make(lastIndex++, (string)list[0], (string)list[1], (string)list[2], (string)list[3], (string)list[4]));
        }

        public void update(int id, params object[] list)
        {
            Student newStudent = null;

            foreach (Student student in students)
            {
                if (student.getId() == id)
                {
                    newStudent = student;
                    students.Remove(student);
                    break;
                }
            }

            if (newStudent != null)
            {
                newStudent.setCodeMelli((string)list[0]);
                newStudent.setFirstName((string)list[1]);
                newStudent.setLastName((string)list[2]);
                newStudent.setCode((string)list[3]);
                newStudent.setField((string)list[4]);

                students.Add(newStudent);
            }
        }

        public void delete(int id)
        {
            foreach (Student student in students)
            {
                if (student.getId() == id)
                {
                    students.Remove(student);
                }
            }
        }

        public void deleteAt(int index)
        {
            students.RemoveAt(index);
        }
    }
}
