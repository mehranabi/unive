﻿using MySql.Data.MySqlClient;
using SamaSystem.models;
using SamaSystem.services;
using System;
using System.Collections.Generic;

namespace SamaSystem.repositories
{
    class GradesRepository : Repository<Grade>
    {
        private static GradesRepository repository = null;

        public static GradesRepository Get()
        {
            if (repository == null)
            {
                repository = new GradesRepository();
            }

            return repository;
        }

        private GradesRepository()
        {
            Program.database.Write(Queries.CREATE_GRADES_TABLE);
        }

        public List<Grade> all()
        {
            List<Grade> grades = new List<Grade>();

            string query = "SELECT " +
                "grades.id as grade_id, grades.grade as grade_grade, grades.semester as grade_semester, " +
                "students.id as student_id, students.code_melli as student_code_melli, students.first_name as student_first_name, students.last_name as student_last_name, students.code as student_code, students.field as student_field, " +
                "teachers.id as teacher_id, teachers.code_melli as teacher_code_melli, teachers.first_name as teacher_first_name, teachers.last_name as teacher_last_name, teachers.code as teacher_code, teachers.level as teacher_level, teachers.salary as teacher_salary, " +
                "courses.id as course_id, courses.name as course_name, courses.type as course_type, courses.unit as course_unit " +
                "FROM grades " +
                "JOIN students ON grades.student_id=students.id " +
                "JOIN teachers ON grades.teacher_id=teachers.id " +
                "JOIN courses ON grades.course_id=courses.id";

            MySqlDataReader reader = Program.database.Read(query);
            while (reader.Read())
            {
                Student student = Student.make((int)reader["student_id"], (string)reader["student_code_melli"], (string)reader["student_first_name"], (string)reader["student_last_name"], (string)reader["student_code"], (string)reader["student_field"]);
                Teacher teacher = Teacher.make((int)reader["teacher_id"], (string)reader["teacher_code_melli"], (string)reader["teacher_first_name"], (string)reader["teacher_last_name"], (string)reader["teacher_code"], (string)reader["teacher_level"], (int)reader["teacher_salary"]);
                Course course = Course.make((int)reader["course_id"], (string)reader["course_name"], (string)reader["course_type"], (int)reader["course_unit"]);
                grades.Add(Grade.make((int)reader["grade_id"], course, student, teacher, (string)reader["grade_semester"], (int)reader["grade_grade"]));
            }
            reader.Close();

            return grades;
        }

        public void delete(int id)
        {
            Program.database.Write(string.Format(Queries.DELETE, Queries.GRADES_TABLE_NAME, id));
        }

        public void deleteAt(int index)
        {
            List<Grade> grades = all();
            int id = grades[index].getId();
            delete(id);
        }

        public Grade find(int id)
        {
            throw new NotImplementedException();
        }

        public Grade find(string prop, object val)
        {
            throw new NotImplementedException();
        }

        public void insert(params object[] list)
        {
            Program.database.Write(string.Format(Queries.INSERT_GRADE, list[0], list[1], list[2], list[3], list[4]));
        }

        public void update(int id, params object[] list)
        {
            throw new NotImplementedException();
        }

        public List<Grade> gradesOf(int id)
        {
            List<Grade> grades = new List<Grade>();

            string query = "SELECT " +
                "grades.id as grade_id, grades.grade as grade_grade, grades.semester as grade_semester, " +
                "students.id as student_id, students.code_melli as student_code_melli, students.first_name as student_first_name, students.last_name as student_last_name, students.code as student_code, students.field as student_field, " +
                "teachers.id as teacher_id, teachers.code_melli as teacher_code_melli, teachers.first_name as teacher_first_name, teachers.last_name as teacher_last_name, teachers.code as teacher_code, teachers.level as teacher_level, teachers.salary as teacher_salary, " +
                "courses.id as course_id, courses.name as course_name, courses.type as course_type, courses.unit as course_unit " +
                "FROM grades " +
                "JOIN students ON grades.student_id=students.id " +
                "JOIN teachers ON grades.teacher_id=teachers.id " +
                "JOIN courses ON grades.course_id=courses.id " +
                "WHERE grades.student_id={0}";
            query = string.Format(query, id);

            MySqlDataReader reader = Program.database.Read(query);
            reader.Read();
            while (reader.Read())
            {
                Student student = Student.make((int)reader["student_id"], (string)reader["student_code_melli"], (string)reader["student_first_name"], (string)reader["student_last_name"], (string)reader["student_code"], (string)reader["student_field"]);
                Teacher teacher = Teacher.make((int)reader["teacher_id"], (string)reader["teacher_code_melli"], (string)reader["teacher_first_name"], (string)reader["teacher_last_name"], (string)reader["teacher_code"], (string)reader["teacher_level"], (int)reader["teacher_salary"]);
                Course course = Course.make((int)reader["course_id"], (string)reader["course_name"], (string)reader["course_type"], (int)reader["course_unit"]);
                grades.Add(Grade.make((int)reader["grade_id"], course, student, teacher, (string)reader["grade_semester"], (int)reader["grade_grade"]));
            }
            reader.Close();

            return grades;
        }
    }
}
