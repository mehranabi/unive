﻿using SamaSystem.models;
using System;

namespace SamaSystem.services
{
    class Authentication
    {
        public static bool authenticate(string username, string password)
        {
            User user = Program.usersRepo.find("username", username);

            if (user != null && user.getPassword() == password)
            {
                setUser(user);
                return true;
            }

            return false;
        }

        private static void setUser(User user)
        {
            Program.user = user;
            switch (user.getType())
            {
                case "Student":
                    Program.student = Program.studentsRepo.find("code_melli", user.getCodeMelli());
                    break;
                case "Teacher":
                    Program.teacher = Program.teachersRepo.find("code_melli", user.getCodeMelli());
                    break;
                case "Admin":
                    break;
                default:
                    throw new Exception("User type is not valid!!");
            }
        }
    }
}
