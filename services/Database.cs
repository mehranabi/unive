﻿using MySql.Data.MySqlClient;

namespace SamaSystem.services
{
    public class Queries
    {
        public static string CONNECTION = "SERVER=localhost;DATABASE=samasystem;UID=samauser;PASSWORD=123456;";

        public static string USERS_TABLE_NAME = "users";
        public static string STUDENTS_TABLE_NAME = "students";
        public static string TEACHERS_TABLE_NAME = "teachers";
        public static string COURSES_TABLE_NAME = "courses";
        public static string GRADES_TABLE_NAME = "grades";

        public static string SELECT_ALL = "SELECT * FROM {0}";
        public static string SELECT_ONE = "SELECT * FROM {0} WHERE id={1}";
        public static string SELECT_ONE_BY = "SELECT * FROM {0} WHERE {1}=\"{2}\"";
        public static string DELETE = "DELETE FROM {0} WHERE id={1};";

        public static string CREATE_USERS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + USERS_TABLE_NAME + " (" +
                "id int NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                "code_melli varchar(10) NOT NULL UNIQUE, " +
                "username varchar(255) NOT NULL UNIQUE, " +
                "password varchar(255) NOT NULL, " +
                "type varchar(255) NOT NULL" +
            ");";
        public static string INSERT_USER = "INSERT INTO " + USERS_TABLE_NAME + " (code_melli, username, password, type) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\");";
        public static string UPDATE_USER = "UPDATE " + USERS_TABLE_NAME + " SET code_melli=\"{1}\", username=\"{2}\", password=\"{3}\", type=\"{4}\" WHERE id={0}";

        public static string CREATE_STUDENTS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + STUDENTS_TABLE_NAME + " (" +
                "id int NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                "code_melli varchar(10) NOT NULL UNIQUE, " +
                "first_name varchar(255) NOT NULL, " +
                "last_name varchar(255) NOT NULL, " +
                "code varchar(10) NOT NULL UNIQUE, " +
                "field varchar(255) NOT NULL" +
            ");";
        public static string INSERT_STUDENT = "INSERT INTO " + STUDENTS_TABLE_NAME + " (code_melli, first_name, last_name, code, field) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\");";
        public static string UPDATE_STUDENT = "UPDATE " + STUDENTS_TABLE_NAME + " SET code_melli=\"{1}\", first_name=\"{2}\", last_name=\"{3}\", code=\"{4}\", field=\"{5}\" WHERE id={0}";

        public static string CREATE_TEACHERS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TEACHERS_TABLE_NAME + " (" +
                "id int NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                "code_melli varchar(10) NOT NULL UNIQUE, " +
                "first_name varchar(255) NOT NULL, " +
                "last_name varchar(255) NOT NULL, " +
                "code varchar(10) NOT NULL UNIQUE, " +
                "level varchar(255) NOT NULL, " +
                "salary int NOT NULL" +
            ");";
        public static string INSERT_TEACHER = "INSERT INTO " + TEACHERS_TABLE_NAME + " (code_melli, first_name, last_name, code, level, salary) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", {5});";
        public static string UPDATE_TEACHER = "UPDATE " + TEACHERS_TABLE_NAME + " SET code_melli=\"{1}\", first_name=\"{2}\", last_name=\"{3}\", code=\"{4}\", level=\"{5}\", salary=\"{6}\" WHERE id={0}";

        public static string CREATE_COURSES_TABLE =
            "CREATE TABLE IF NOT EXISTS " + COURSES_TABLE_NAME + " (" +
                "id int NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                "name varchar(255) NOT NULL UNIQUE, " +
                "type varchar(255) NOT NULL, " +
                "unit int NOT NULL " +
            ");";
        public static string INSERT_COURSE = "INSERT INTO " + COURSES_TABLE_NAME + " (name, type, unit) VALUES (\"{0}\", \"{1}\", {2});";
        public static string UPDATE_COURSE = "UPDATE " + COURSES_TABLE_NAME + " SET name=\"{1}\", type=\"{2}\", unit={3} WHERE id={0}";

        public static string CREATE_GRADES_TABLE =
            "CREATE TABLE IF NOT EXISTS " + GRADES_TABLE_NAME + " (" +
                "id int NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                "course_id int NOT NULL, " +
                "student_id int NOT NULL, " +
                "teacher_id int NOT NULL, " +
                "grade int NOT NULL," +
                "semester varchar(255) NOT NULL" +
            ");";
        public static string INSERT_GRADE = "INSERT INTO " + GRADES_TABLE_NAME + " (course_id, student_id, teacher_id, semester, grade) VALUES ({0}, {1}, {2}, {3}, \"{4}\");";
        public static string UPDATE_GRADE = "UPDATE " + GRADES_TABLE_NAME + " SET course_id={1}, student_id={2}, teacher_id={3}, semester=\"{4}\", grade=\"{5}\" WHERE id={0}";
    }

    class Database
    {
        private MySqlConnection connection;

        public Database()
        {
            connection = new MySqlConnection(Queries.CONNECTION);
            Close();
            Open();
        }

        public void Open()
        {
            connection.Open();
        }

        public void Close()
        {
            connection.Close();
        }

        public void Write(string query)
        {
            new MySqlCommand(query, connection).ExecuteNonQuery();
        }

        public MySqlDataReader Read(string query)
        {
            return new MySqlCommand(query, connection).ExecuteReader();
        }
    }
}
