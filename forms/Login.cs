﻿using SamaSystem.services;
using System;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            if (username_tb.Text.Length == 0)
            {
                MessageBox.Show("Please enter your username!");
                return;
            }

            if (password_tb.Text.Length == 0)
            {
                MessageBox.Show("Please enter your password!");
                return;
            }

            string username = username_tb.Text;
            string password = password_tb.Text;

            bool loggedin = Authentication.authenticate(username, password);

            if (!loggedin)
            {
                MessageBox.Show("Username or Password is incorrect!");
                return;
            }

            if (Program.user.isAdmin())
            {
                Program.RunAdmin();
                return;
            }

            if (Program.user.getType() == "Student")
            {
                Program.RunStudent();
            } else if (Program.user.getType() == "Teacher")
            {
                Program.RunTeacher();
            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.database.Close();
        }
    }
}
