﻿using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class TeacherPanel : Form
    {
        public TeacherPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            new CreateGrade(Program.teacher).ShowDialog();
        }

        private void TeacherPanel_Load(object sender, System.EventArgs e)
        {
            //welcome.Text = string.Format("Welcome {0}", Program.teacher.getFullName());
        }
    }
}
