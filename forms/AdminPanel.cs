﻿using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class AdminPanel : Form
    {
        public AdminPanel()
        {
            InitializeComponent();
        }

        private void users_btn_Click(object sender, System.EventArgs e)
        {
            new ModelsList("Users List", "User").ShowDialog();
        }

        private void teachers_btn_Click(object sender, System.EventArgs e)
        {
            new ModelsList("Teachers List", "Teacher").ShowDialog();
        }

        private void students_btn_Click(object sender, System.EventArgs e)
        {
            new ModelsList("Students List", "Student").ShowDialog();
        }

        private void courses_btn_Click(object sender, System.EventArgs e)
        {
            new ModelsList("Courses List", "Course").ShowDialog();
        }

        private void grades_btn_Click(object sender, System.EventArgs e)
        {
            new ModelsList("Grades List", "Grade").ShowDialog();
        }
    }
}
