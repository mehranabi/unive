﻿using SamaSystem.models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class ModelsList : Form
    {
        private string type;
        private int selectedIndex = -1;

        public ModelsList()
        {
            InitializeComponent();
        }

        public ModelsList(string title, string type)
        {
            InitializeComponent();

            this.type = type;
            Text = title;
        }

        private void ModelsList_Load(object sender, EventArgs e)
        {
            list.Items.Clear();

            switch (type)
            {
                case "User":
                    Program.usersRepo.all().ForEach(u =>
                    {
                        list.Items.Add(u.getUsername());
                    });
                    break;
                case "Student":
                    Program.studentsRepo.all().ForEach(s =>
                    {
                        list.Items.Add(s.getFullName());
                    });
                    break;
                case "Teacher":
                    Program.teachersRepo.all().ForEach(s =>
                    {
                        list.Items.Add(s.getFullName());
                    });
                    break;
                case "Course":
                    Program.coursesRepo.all().ForEach(c =>
                    {
                        list.Items.Add(c.getName());
                    });
                    break;
                case "Grade":
                    Program.gradesRepo.all().ForEach(g =>
                    {
                        list.Items.Add(string.Format("Teacher: {0} | Course: {1} | Student: {2} | Grade: {3} | Semester: {4}", g.getTeacher().getFullName(), g.getCourse().getName(), g.getStudent().getFullName(), g.getGrade(), g.getSemester()));
                    });
                    break;
                default:
                    throw new Exception("Type is invalid!");
            }
        }

        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedIndex = list.SelectedIndex;

            if (selectedIndex > -1)
            {
                remove_btn.Enabled = true;
                edit_btn.Enabled = true;
            }
            else
            {
                remove_btn.Enabled = false;
                edit_btn.Enabled = false;
            }
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            switch (type)
            {
                case "User":
                    User user = Program.usersRepo.all()[selectedIndex];
                    new CreateUser(user).ShowDialog();
                    break;
                case "Student":
                    break;
                case "Teacher":
                    break;
                case "Course":
                    Course course = Program.coursesRepo.all()[selectedIndex];
                    new CreateCourse(course).ShowDialog();
                    break;
                case "Grade":
                    break;
                default:
                    throw new Exception("Type is invalid!");
            }
        }

        private void remove_btn_Click(object sender, EventArgs e)
        {
            switch (type)
            {
                case "User":
                    Program.usersRepo.deleteAt(selectedIndex);
                    break;
                case "Student":
                    Program.studentsRepo.deleteAt(selectedIndex);
                    break;
                case "Teacher":
                    Program.teachersRepo.deleteAt(selectedIndex);
                    break;
                case "Course":
                    Program.coursesRepo.deleteAt(selectedIndex);
                    break;
                case "Grade":
                    Program.gradesRepo.deleteAt(selectedIndex);
                    break;
                default:
                    throw new Exception("Type is invalid!");
            }
            ModelsList_Load(null, null);
        }

        private void search_btn_Click(object sender, EventArgs e)
        {
            List<string> myData = new List<string>();
            switch (type)
            {
                case "User":
                    Program.usersRepo.all().ForEach(u =>
                    {
                        myData.Add(u.getUsername());
                    });
                    break;
                case "Student":
                    Program.studentsRepo.all().ForEach(s =>
                    {
                        myData.Add(s.getFullName());
                    });
                    break;
                case "Teacher":
                    Program.teachersRepo.all().ForEach(s =>
                    {
                        myData.Add(s.getFullName());
                    });
                    break;
                case "Course":
                    Program.coursesRepo.all().ForEach(c =>
                    {
                        myData.Add(c.getName());
                    });
                    break;
                case "Grade":
                    Program.gradesRepo.all().ForEach(g =>
                    {
                        myData.Add(string.Format("Teacher: {0} Course: {1} Student: {2} Grade: {3} Semester: {4}", g.getTeacher().getFullName(), g.getCourse().getName(), g.getStudent().getFullName(), g.getGrade(), g.getSemester()));
                    });
                    break;
                default:
                    throw new Exception("Type is invalid!");
            }

            list.BeginUpdate();
            list.Items.Clear();

            if (!string.IsNullOrEmpty(search_tb.Text))
            {
                foreach (string str in myData)
                {
                    if (str.Contains(search_tb.Text))
                    {
                        list.Items.Add(str);
                    }
                }
            }
            else
            {
                list.Items.AddRange(myData.ToArray());
            }

            list.EndUpdate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (type)
            {
                case "User":
                    new CreateUser().ShowDialog();
                    break;
                case "Student":
                    new CreateStudent().ShowDialog();
                    break;
                case "Teacher":
                    new CreateTeacher().ShowDialog();
                    break;
                case "Course":
                    new CreateCourse().ShowDialog();
                    break;
                case "Grade":
                    new CreateGrade().ShowDialog();
                    break;
                default:
                    throw new Exception("Type is invalid!");
            }
        }

        private void refresh_btn_Click(object sender, EventArgs e)
        {
            ModelsList_Load(null, null);
        }
    }
}
