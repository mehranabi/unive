﻿using SamaSystem.models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class CreateGrade : Form
    {
        private List<Teacher> teachers;
        private List<Student> students;
        private List<Course> courses;

        private Teacher teacher = null;

        public CreateGrade()
        {
            InitializeComponent();
        }

        public CreateGrade(Teacher teacher)
        {
            InitializeComponent();
            this.teacher = teacher;
        }

        private void CreateGrade_Load(object sender, EventArgs e)
        {
            teachers = Program.teachersRepo.all();
            students = Program.studentsRepo.all();
            courses = Program.coursesRepo.all();

            if (teacher != null)
            {
                teachers_cb.Items.Add(teacher.getFullName());
            } else
            {
                teachers.ForEach(t => teachers_cb.Items.Add(t.getFullName()));
            }

            students.ForEach(t => students_cb.Items.Add(t.getFullName()));
            courses.ForEach(t => courses_cb.Items.Add(t.getName()));
        }

        private void submit_Click(object sender, EventArgs e)
        {
            Teacher teacher = teachers[teachers_cb.SelectedIndex];
            Student student = students[students_cb.SelectedIndex];
            Course course = courses[courses_cb.SelectedIndex];
            byte grade = byte.Parse(grade_tb.Text);
            string semester = semester_tb.Text;

            Program.gradesRepo.insert(course.getId(), student.getId(), teacher.getId(), semester, grade);

            MessageBox.Show("New grade has been submitted!");
        }
    }
}
