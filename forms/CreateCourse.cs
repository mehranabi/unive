﻿using SamaSystem.models;
using System;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class CreateCourse : Form
    {
        private Course course = null;

        public CreateCourse()
        {
            InitializeComponent();
        }

        public CreateCourse(Course course)
        {
            InitializeComponent();
            this.course = course;
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string name = name_tb.Text;
            string type = type_tb.Text;
            byte unit = byte.Parse(units_tb.Text);

            if (name.Length == 0 || type.Length == 0 || unit == 0)
            {
                MessageBox.Show("Plz fill out all fields!");
                return;
            }

            if (course != null)
            {
                Program.coursesRepo.update(course.getId(), name, type, unit);
            } else
            {
                Program.coursesRepo.insert(name, type, unit);
            }

            MessageBox.Show("New course has been submitted!");
        }

        private void CreateCourse_Load(object sender, EventArgs e)
        {
            if (course != null)
            {
                name_tb.Text = course.getName();
                type_tb.Text = course.getType();
                units_tb.Text = course.getUnit().ToString();
            }
        }
    }
}
