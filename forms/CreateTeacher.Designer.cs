﻿namespace SamaSystem.forms
{
    partial class CreateTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.name_tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.family_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.code_melli_tb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.code_tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.level_tb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.salary_tb = new System.Windows.Forms.TextBox();
            this.submit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // name_tb
            // 
            this.name_tb.Location = new System.Drawing.Point(16, 34);
            this.name_tb.Name = "name_tb";
            this.name_tb.Size = new System.Drawing.Size(176, 22);
            this.name_tb.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Family";
            // 
            // family_tb
            // 
            this.family_tb.Location = new System.Drawing.Point(16, 79);
            this.family_tb.Name = "family_tb";
            this.family_tb.Size = new System.Drawing.Size(176, 22);
            this.family_tb.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Code Melli";
            // 
            // code_melli_tb
            // 
            this.code_melli_tb.Location = new System.Drawing.Point(213, 33);
            this.code_melli_tb.Name = "code_melli_tb";
            this.code_melli_tb.Size = new System.Drawing.Size(147, 22);
            this.code_melli_tb.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(213, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Teacher Code";
            // 
            // code_tb
            // 
            this.code_tb.Location = new System.Drawing.Point(216, 83);
            this.code_tb.Name = "code_tb";
            this.code_tb.Size = new System.Drawing.Size(144, 22);
            this.code_tb.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Level";
            // 
            // level_tb
            // 
            this.level_tb.Location = new System.Drawing.Point(19, 129);
            this.level_tb.Name = "level_tb";
            this.level_tb.Size = new System.Drawing.Size(173, 22);
            this.level_tb.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(216, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Salary";
            // 
            // salary_tb
            // 
            this.salary_tb.Location = new System.Drawing.Point(216, 129);
            this.salary_tb.Name = "salary_tb";
            this.salary_tb.Size = new System.Drawing.Size(144, 22);
            this.salary_tb.TabIndex = 11;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(398, 58);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(91, 73);
            this.submit.TabIndex = 12;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // CreateTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 172);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.salary_tb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.level_tb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.code_tb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.code_melli_tb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.family_tb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name_tb);
            this.Controls.Add(this.label1);
            this.Name = "CreateTeacher";
            this.Text = "Create/Edit Teacher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox family_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox code_melli_tb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox code_tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox level_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox salary_tb;
        private System.Windows.Forms.Button submit;
    }
}