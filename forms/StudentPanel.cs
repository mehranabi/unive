﻿using SamaSystem.models;
using SamaSystem.repositories;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class StudentPanel : Form
    {
        public StudentPanel()
        {
            InitializeComponent();
        }

        private void StudentPanel_Load(object sender, System.EventArgs e)
        {
            //welcome.Text = Program.student.getFullName();
        }

        private void load_btn_Click(object sender, System.EventArgs e)
        {
            GradesRepository repo = (GradesRepository)Program.gradesRepo;
            List<Grade> grades = repo.gradesOf(Program.student.getId());
            grades.ForEach(g => list.Items.Add(string.Format("Semester: {0} | Course: {1} | Grade: {2} | Teacher: {3}", g.getSemester(), g.getCourse().getName(), g.getGrade(), g.getTeacher().getFullName())));
        }
    }
}
