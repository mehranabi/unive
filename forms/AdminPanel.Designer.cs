﻿namespace SamaSystem.forms
{
    partial class AdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.users_btn = new System.Windows.Forms.Button();
            this.teachers_btn = new System.Windows.Forms.Button();
            this.students_btn = new System.Windows.Forms.Button();
            this.courses_btn = new System.Windows.Forms.Button();
            this.grades_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // users_btn
            // 
            this.users_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.users_btn.Location = new System.Drawing.Point(13, 13);
            this.users_btn.Name = "users_btn";
            this.users_btn.Size = new System.Drawing.Size(150, 100);
            this.users_btn.TabIndex = 0;
            this.users_btn.Text = "Users";
            this.users_btn.UseVisualStyleBackColor = true;
            this.users_btn.Click += new System.EventHandler(this.users_btn_Click);
            // 
            // teachers_btn
            // 
            this.teachers_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teachers_btn.Location = new System.Drawing.Point(169, 13);
            this.teachers_btn.Name = "teachers_btn";
            this.teachers_btn.Size = new System.Drawing.Size(150, 100);
            this.teachers_btn.TabIndex = 0;
            this.teachers_btn.Text = "Teachers";
            this.teachers_btn.UseVisualStyleBackColor = true;
            this.teachers_btn.Click += new System.EventHandler(this.teachers_btn_Click);
            // 
            // students_btn
            // 
            this.students_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.students_btn.Location = new System.Drawing.Point(325, 13);
            this.students_btn.Name = "students_btn";
            this.students_btn.Size = new System.Drawing.Size(150, 100);
            this.students_btn.TabIndex = 0;
            this.students_btn.Text = "Students";
            this.students_btn.UseVisualStyleBackColor = true;
            this.students_btn.Click += new System.EventHandler(this.students_btn_Click);
            // 
            // courses_btn
            // 
            this.courses_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.courses_btn.Location = new System.Drawing.Point(12, 119);
            this.courses_btn.Name = "courses_btn";
            this.courses_btn.Size = new System.Drawing.Size(150, 100);
            this.courses_btn.TabIndex = 0;
            this.courses_btn.Text = "Courses";
            this.courses_btn.UseVisualStyleBackColor = true;
            this.courses_btn.Click += new System.EventHandler(this.courses_btn_Click);
            // 
            // grades_btn
            // 
            this.grades_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grades_btn.Location = new System.Drawing.Point(169, 119);
            this.grades_btn.Name = "grades_btn";
            this.grades_btn.Size = new System.Drawing.Size(150, 100);
            this.grades_btn.TabIndex = 0;
            this.grades_btn.Text = "Grades";
            this.grades_btn.UseVisualStyleBackColor = true;
            this.grades_btn.Click += new System.EventHandler(this.grades_btn_Click);
            // 
            // AdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 230);
            this.Controls.Add(this.grades_btn);
            this.Controls.Add(this.courses_btn);
            this.Controls.Add(this.students_btn);
            this.Controls.Add(this.teachers_btn);
            this.Controls.Add(this.users_btn);
            this.Name = "AdminPanel";
            this.Text = "Admin Panel";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button users_btn;
        private System.Windows.Forms.Button teachers_btn;
        private System.Windows.Forms.Button students_btn;
        private System.Windows.Forms.Button courses_btn;
        private System.Windows.Forms.Button grades_btn;
    }
}