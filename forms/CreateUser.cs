﻿using SamaSystem.models;
using System;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class CreateUser : Form
    {
        private User user = null;

        public CreateUser()
        {
            InitializeComponent();
        }

        public CreateUser(User user)
        {
            InitializeComponent();
            this.user = user;
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string codeMelli = code_melli_tb.Text;
            string username = username_tb.Text;
            string password = password_tb.Text;
            string type = type_cb.SelectedItem.ToString();

            if (codeMelli.Length == 0 || username.Length == 0 || password.Length == 0 || type.Length == 0)
            {
                MessageBox.Show("Fill out all fields!");
                return;
            }

            if (user != null)
            {
                Program.usersRepo.update(user.getId(), codeMelli, username, password, type);
            } else
            {
                Program.usersRepo.insert(codeMelli, username, password, type);
            }

            MessageBox.Show("User has been created successfully!");
        }

        private void CreateUser_Load(object sender, EventArgs e)
        {
            if (user != null)
            {
                code_melli_tb.Text = user.getCodeMelli();
                username_tb.Text = user.getUsername();
                password_tb.Text = user.getPassword();
                type_cb.SelectedItem = user.GetType();
            }
        }
    }
}
