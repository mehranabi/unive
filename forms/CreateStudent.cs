﻿using System;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class CreateStudent : Form
    {
        public CreateStudent()
        {
            InitializeComponent();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string name = name_tb.Text;
            string family = family_tb.Text;
            string code_melli = code_melli_tb.Text;
            string code = code_tb.Text;
            string field = field_tb.Text;

            if (name.Length == 0 || family.Length == 0 || code_melli.Length == 0 || code.Length == 0 || field.Length == 0)
            {
                MessageBox.Show("Plz fill out all fields!");
                return;
            }

            Program.studentsRepo.insert(code_melli, name, family, code, field);

            MessageBox.Show("Student has been created successfully.");
        }
    }
}
