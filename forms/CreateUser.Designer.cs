﻿namespace SamaSystem.forms
{
    partial class CreateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.code_melli_tb = new System.Windows.Forms.TextBox();
            this.Family = new System.Windows.Forms.Label();
            this.username_tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.password_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.type_cb = new System.Windows.Forms.ComboBox();
            this.submit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code Melli";
            // 
            // code_melli_tb
            // 
            this.code_melli_tb.Location = new System.Drawing.Point(16, 34);
            this.code_melli_tb.Name = "code_melli_tb";
            this.code_melli_tb.Size = new System.Drawing.Size(149, 22);
            this.code_melli_tb.TabIndex = 1;
            // 
            // Family
            // 
            this.Family.AutoSize = true;
            this.Family.Location = new System.Drawing.Point(16, 63);
            this.Family.Name = "Family";
            this.Family.Size = new System.Drawing.Size(73, 17);
            this.Family.TabIndex = 2;
            this.Family.Text = "Username";
            // 
            // username_tb
            // 
            this.username_tb.Location = new System.Drawing.Point(19, 84);
            this.username_tb.Name = "username_tb";
            this.username_tb.Size = new System.Drawing.Size(146, 22);
            this.username_tb.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(184, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // password_tb
            // 
            this.password_tb.Location = new System.Drawing.Point(187, 33);
            this.password_tb.Name = "password_tb";
            this.password_tb.Size = new System.Drawing.Size(152, 22);
            this.password_tb.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(187, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Type";
            // 
            // type_cb
            // 
            this.type_cb.FormattingEnabled = true;
            this.type_cb.Items.AddRange(new object[] {
            "Admin",
            "Student",
            "Teacher"});
            this.type_cb.Location = new System.Drawing.Point(187, 80);
            this.type_cb.Name = "type_cb";
            this.type_cb.Size = new System.Drawing.Size(152, 24);
            this.type_cb.TabIndex = 7;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(374, 43);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(91, 57);
            this.submit.TabIndex = 8;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // CreateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 127);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.type_cb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.password_tb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.username_tb);
            this.Controls.Add(this.Family);
            this.Controls.Add(this.code_melli_tb);
            this.Controls.Add(this.label1);
            this.Name = "CreateUser";
            this.Text = "Create/Edit User";
            this.Load += new System.EventHandler(this.CreateUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox code_melli_tb;
        private System.Windows.Forms.Label Family;
        private System.Windows.Forms.TextBox username_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox password_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox type_cb;
        private System.Windows.Forms.Button submit;
    }
}