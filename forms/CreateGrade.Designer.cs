﻿namespace SamaSystem.forms
{
    partial class CreateGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.teachers_cb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.students_cb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.courses_cb = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grade_tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.semester_tb = new System.Windows.Forms.TextBox();
            this.submit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teacher";
            // 
            // teachers_cb
            // 
            this.teachers_cb.FormattingEnabled = true;
            this.teachers_cb.Location = new System.Drawing.Point(16, 34);
            this.teachers_cb.Name = "teachers_cb";
            this.teachers_cb.Size = new System.Drawing.Size(233, 24);
            this.teachers_cb.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Student";
            // 
            // students_cb
            // 
            this.students_cb.FormattingEnabled = true;
            this.students_cb.Location = new System.Drawing.Point(19, 86);
            this.students_cb.Name = "students_cb";
            this.students_cb.Size = new System.Drawing.Size(230, 24);
            this.students_cb.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(296, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Course";
            // 
            // courses_cb
            // 
            this.courses_cb.FormattingEnabled = true;
            this.courses_cb.Location = new System.Drawing.Point(299, 34);
            this.courses_cb.Name = "courses_cb";
            this.courses_cb.Size = new System.Drawing.Size(209, 24);
            this.courses_cb.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(296, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Grade";
            // 
            // grade_tb
            // 
            this.grade_tb.Location = new System.Drawing.Point(296, 86);
            this.grade_tb.Name = "grade_tb";
            this.grade_tb.Size = new System.Drawing.Size(100, 22);
            this.grade_tb.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Semester";
            // 
            // semester_tb
            // 
            this.semester_tb.Location = new System.Drawing.Point(22, 138);
            this.semester_tb.Name = "semester_tb";
            this.semester_tb.Size = new System.Drawing.Size(100, 22);
            this.semester_tb.TabIndex = 9;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(296, 137);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(75, 23);
            this.submit.TabIndex = 10;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // CreateGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 188);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.semester_tb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grade_tb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.courses_cb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.students_cb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.teachers_cb);
            this.Controls.Add(this.label1);
            this.Name = "CreateGrade";
            this.Text = "Create\\Edit Grade";
            this.Load += new System.EventHandler(this.CreateGrade_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox teachers_cb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox students_cb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox courses_cb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox grade_tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox semester_tb;
        private System.Windows.Forms.Button submit;
    }
}