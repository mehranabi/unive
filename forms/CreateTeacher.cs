﻿using System;
using System.Windows.Forms;

namespace SamaSystem.forms
{
    public partial class CreateTeacher : Form
    {
        public CreateTeacher()
        {
            InitializeComponent();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string name = name_tb.Text;
            string family = family_tb.Text;
            string code_melli = code_melli_tb.Text;
            string code = code_tb.Text;
            string level = level_tb.Text;
            uint salary = uint.Parse(salary_tb.Text);

            if (name.Length == 0 || family.Length == 0 || code_melli.Length == 0 || code.Length == 0 || level.Length == 0 || salary == 0)
            {
                MessageBox.Show("Plz fill out all fields!");
                return;
            }

            Program.teachersRepo.insert(code_melli, name, family, code, level, salary);

            MessageBox.Show("Teacher has been created successfully.");
        }
    }
}
