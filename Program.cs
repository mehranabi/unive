﻿using SamaSystem.forms;
using SamaSystem.models;
using SamaSystem.repositories;
using SamaSystem.services;
using System.Windows.Forms;

namespace SamaSystem
{
    static class Program
    {
        public static Repository<User> usersRepo;
        public static Repository<Student> studentsRepo;
        public static Repository<Teacher> teachersRepo;
        public static Repository<Course> coursesRepo;
        public static Repository<Grade> gradesRepo;

        public static User user = null;
        public static Student student = null;
        public static Teacher teacher = null;

        public static Database database = new Database();

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            usersRepo = UsersRepository.Get();
            studentsRepo = StudentsRepository.Get();
            teachersRepo = TeachersRepository.Get();
            coursesRepo = CoursesRepository.Get();
            gradesRepo = GradesRepository.Get();

            Application.Run(new Login());
        }

        public static void RunStudent()
        {
            new StudentPanel().ShowDialog();
        }

        public static void RunTeacher()
        {
            new TeacherPanel().ShowDialog();
        }

        public static void RunAdmin()
        {
            new AdminPanel().ShowDialog();
        }
    }
}
