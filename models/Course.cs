﻿using SamaSystem.utils;
using System;

namespace SamaSystem.models
{
    public class Course : Model
    {
        private int id;
        private string name;
        private string type;
        private byte unit;

        public static Course make(int id, string name, string type, int unit)
        {
            Course course = new Course();
            course.setId(id);
            course.setName(name);
            course.setType(type);
            course.setUnit((byte)unit);
            return course;
        }

        public void setName(string name)
        {
            if (!Validation.Name(name))
            {
                throw new Exception("Invalid input!");
            }

            this.name = name;
        }

        public void setType(string type)
        {
            if (!Validation.Name(type))
            {
                throw new Exception("Invalid input!");
            }

            this.type = type;
        }

        public void setUnit(byte unit)
        {
            this.unit = unit;
        }

        public string getName()
        {
            return name;
        }

        public string getType()
        {
            return type;
        }

        public byte getUnit()
        {
            return unit;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public object get(string prop)
        {
            switch(prop)
            {
                case "name":
                    return getName();
                case "type":
                    return getType();
                case "unit":
                    return getUnit();
                default:
                    return null;
            }
        }
    }
}
