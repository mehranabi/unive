﻿using SamaSystem.utils;
using System;

namespace SamaSystem.models
{
    public class Person
    {
        private string code_melli;
        private string first_name;
        private string last_name;

        public void setCodeMelli(string code_melli)
        {
            if (!Validation.CodeMelli(code_melli))
            {
                throw new Exception("Invalid input!");
            }

            this.code_melli = code_melli;
        }

        public void setFirstName(string first_name)
        {
            if (!Validation.Name(first_name))
            {
                throw new Exception("Invalid input!");
            }

            this.first_name = first_name;
        }

        public void setLastName(string last_name)
        {
            if (!Validation.Name(last_name))
            {
                throw new Exception("Invalid input!");
            }

            this.last_name = last_name;
        }

        public string getCodeMelli()
        {
            return code_melli;
        }

        public string getFirstName()
        {
            return first_name;
        }

        public string getLastName()
        {
            return last_name;
        }

        public string getFullName()
        {
            return string.Format("{0} {1}", first_name, last_name);
        }
    }
}
