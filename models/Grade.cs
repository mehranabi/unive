﻿using SamaSystem.utils;
using System;

namespace SamaSystem.models
{
    class Grade : Model
    {
        private int id;
        private Course course;
        private Student student;
        private Teacher teacher;
        private string semester;
        private byte grade;

        public static Grade make(int id, Course course, Student student, Teacher teacher, string semseter, int grade)
        {
            Grade gradeModel = new Grade();
            gradeModel.setId(id);
            gradeModel.setCourse(course);
            gradeModel.setStudent(student);
            gradeModel.setTeacher(teacher);
            gradeModel.setSemester(semseter);
            gradeModel.setGrade((byte)grade);
            return gradeModel;
        }

        public void setCourse(Course course)
        {
            this.course = course;
        }

        public void setStudent(Student student)
        {
            this.student = student;
        }

        public void setTeacher(Teacher teacher)
        {
            this.teacher = teacher;
        }

        public void setSemester(string semester)
        {
            this.semester = semester;
        }

        public void setGrade(byte grade)
        {
            if (!Validation.Grade(grade))
            {
                throw new Exception("Invalid input!");
            }

            this.grade = grade;
        }

        public Course getCourse()
        {
            return course;
        }

        public Student getStudent()
        {
            return student;
        }

        public Teacher getTeacher()
        {
            return teacher;
        }

        public string getSemester()
        {
            return semester;
        }

        public byte getGrade()
        {
            return grade;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public object get(string prop)
        {
            switch(prop)
            {
                case "course":
                    return getCourse();
                case "student":
                    return getStudent();
                case "teacher":
                    return getTeacher();
                case "semseter":
                    return getSemester();
                case "grade":
                    return getGrade();
                default:
                    return null;
            }
        }
    }
}
