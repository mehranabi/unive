﻿using SamaSystem.utils;
using System;

namespace SamaSystem.models
{
    public class User : Model
    {
        private int id;
        private string code_melli;
        private string username;
        private string password;
        private string type;

        public static User make(int id, string code_melli, string username, string password, string type)
        {
            User user = new User();
            user.setId(id);
            user.setCodeMelli(code_melli);
            user.setUsername(username);
            user.setPassword(password);
            user.setType(type);
            return user;
        }

        public void setCodeMelli(string code_melli)
        {
            if (!Validation.CodeMelli(code_melli))
            {
                throw new Exception("Invalid input!");
            }

            this.code_melli = code_melli;
        }

        public void setUsername(string username)
        {
            if (!Validation.Username(username))
            {
                throw new Exception("Invalid input!");
            }

            this.username = username;
        }

        public void setPassword(string password)
        {
            if (!Validation.Password(password))
            {
                throw new Exception("Invalid input!");
            }

            this.password = password;
        }

        public void setType(string type)
        {
            if (type != "Student" && type != "Teacher" && type != "Admin")
            {
                throw new Exception("Invalid input!");
            }

            this.type = type;
        }

        public void setAsAdmin()
        {
            type = "Admin";
        }

        public bool isAdmin()
        {
            return type == "Admin";
        }

        public string getCodeMelli()
        {
            return code_melli;
        }

        public string getUsername()
        {
            return username;
        }

        public string getPassword()
        {
            return password;
        }

        public string getType()
        {
            return type;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public object get(string property)
        {
            switch (property)
            {
                case "code_melli":
                    return getCodeMelli();
                case "username":
                    return getUsername();
                case "password":
                    return getPassword();
                case "type":
                    return getType();
                case "admin":
                    return isAdmin();
                default:
                    return null;
            }
        }
    }
}
