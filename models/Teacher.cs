﻿using SamaSystem.utils;
using System;

namespace SamaSystem.models
{
    public class Teacher : Person, Model
    {
        private int id;
        private string code;
        private string level;
        private uint salary;

        public static Teacher make(int id, string code_melli, string first_name, string last_name, string code, string level, int salary)
        {
            Teacher teacher = new Teacher();
            teacher.setId(id);
            teacher.setCodeMelli(code_melli);
            teacher.setFirstName(first_name);
            teacher.setLastName(last_name);
            teacher.setCode(code);
            teacher.setLevel(level);
            teacher.setSalary((uint)salary);
            return teacher;
        }

        public void setCode(string code)
        {
            if (!Validation.TeacherCode(code)) {
                throw new Exception("Invalid input!");
            }

            this.code = code;
        }

        public void setLevel(string level)
        {
            if (!Validation.Name(level)) {
                throw new Exception("Invalid input!");
            }

            this.level = level;
        }

        public void setSalary(uint salary)
        {
            this.salary = salary;
        }

        public string getCode()
        {
            return code;
        }

        public string getLevel()
        {
            return level;
        }

        public uint getSalary()
        {
            return salary;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public object get(string prop)
        {
            switch (prop)
            {
                case "code_melli":
                    return getCodeMelli();
                case "first_name":
                    return getFirstName();
                case "last_name":
                    return getLastName();
                case "code":
                    return getCode();
                case "level":
                    return getLevel();
                case "salary":
                    return getSalary();
                default:
                    return null;
            }
        }
    }
}
