﻿namespace SamaSystem.models
{
    public interface Model
    {
        void setId(int id);
        int getId();
        object get(string prop);
    }
}
