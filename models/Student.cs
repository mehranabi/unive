﻿using SamaSystem.utils;
using System;

namespace SamaSystem.models
{
    class Student : Person, Model
    {
        private int id;
        private string code;
        private string field;

        public static Student make(int id, string code_melli, string first_name, string last_name, string code, string field)
        {
            Student student = new Student();
            student.setId(id);
            student.setCodeMelli(code_melli);
            student.setFirstName(first_name);
            student.setLastName(last_name);
            student.setCode(code);
            student.setField(field);
            return student;
        }

        public void setCode(string code) {
            if (!Validation.StudentCode(code))
            {
                throw new Exception("Invalid input!");
            }

            this.code = code;
        }

        public void setField(string field)
        {
            if (!Validation.Name(field))
            {
                throw new Exception("Invalid input!");
            }

            this.field = field;
        }

        public string getCode()
        {
            return code;
        }

        public string getField()
        {
            return field;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public object get(string property)
        {
            switch (property)
            {
                case "id":
                    return getId();
                case "code_melli":
                    return getCodeMelli();
                case "first_name":
                    return getFirstName();
                case "last_name":
                    return getLastName();
                case "code":
                    return getCode();
                case "field":
                    return getField();
                default:
                    return null;
            }
        }
    }
}
